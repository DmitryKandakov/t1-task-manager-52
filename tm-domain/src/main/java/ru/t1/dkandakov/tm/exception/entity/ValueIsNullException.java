package ru.t1.dkandakov.tm.exception.entity;

public final class ValueIsNullException extends AbstractEntityException {

    public ValueIsNullException() {
        super("Error! This value is null...");
    }

}