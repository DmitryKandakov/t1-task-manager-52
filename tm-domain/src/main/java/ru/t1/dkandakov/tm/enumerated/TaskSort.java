package ru.t1.dkandakov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum TaskSort {

    BY_NAME("Sort by name", "NAME"),
    BY_STATUS("Sort by status", "STATUS"),
    BY_CREATED("Sort by created", "CREATED");

    @NotNull
    private final String name;

    @NotNull
    private final String columnName;

    TaskSort(
            @NotNull final String name,
            @NotNull final String columnName
    ) {
        this.name = name;
        this.columnName = columnName;
    }

    @Nullable
    public static TaskSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

}
