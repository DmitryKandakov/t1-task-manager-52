package ru.t1.dkandakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.task.TaskUpdateByIdRequest;
import ru.t1.dkandakov.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME: ");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIdRequest request =
                new TaskUpdateByIdRequest(getToken(), id, name, description);
        getTaskEndpoint().updateTaskById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-update-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update task by id.";
    }

}