package ru.t1.dkandakov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.UserDTO;
import ru.t1.dkandakov.tm.enumerated.Role;

public interface IUserServiceDTO extends IServiceDTO<UserDTO> {

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO removeOneByLogin(@Nullable String login);

    @Nullable
    UserDTO removeOneByEmail(@Nullable String email);

    @NotNull
    UserDTO setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    UserDTO lockOneByLogin(@Nullable String login);

    @NotNull
    UserDTO unlockOneByLogin(@Nullable String login);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

}
