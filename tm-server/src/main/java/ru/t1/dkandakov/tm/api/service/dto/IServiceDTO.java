package ru.t1.dkandakov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.List;

public interface IServiceDTO<M extends AbstractModelDTO> {

    @NotNull
    M add(@Nullable M model);

    @NotNull
    Collection<M> add(@Nullable Collection<M> models);

    @NotNull
    Collection<M> set(@Nullable Collection<M> models);

    @NotNull
    List<M> findAll();

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    @NotNull
    M removeOne(@Nullable M model);

    @Nullable
    M removeOneById(@Nullable String id);

    @Nullable
    M removeOneByIndex(@Nullable Integer index);

    void removeAll();

    long getSize();

    boolean existsById(String id);

}
