package ru.t1.dkandakov.tm.api.repository.dto;

import ru.t1.dkandakov.tm.dto.model.SessionDTO;

public interface ISessionRepositoryDTO extends IUserOwnerRepositoryDTO<SessionDTO> {
}
