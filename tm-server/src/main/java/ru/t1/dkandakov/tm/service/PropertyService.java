package ru.t1.dkandakov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.api.service.IPropertyService;

import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "version";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "3333344";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "2222456";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String EMPTY_VALUE = "---";
    @NotNull
    public static final String SESSION_KEY = "session.key";
    @NotNull
    public static final String SESSION_KEY_DEFAULT = "qwertyu123";
    @NotNull
    public static final String SESSION_TIMEOUT = "session.timeout";
    @NotNull
    public static final String SESSION_TIMEOUT_DEFAULT = "10000";
    @NotNull
    public static final String DB_URL_DEFAULT = "idbc:postgresql://localhost:5432/tm";
    @NotNull
    public static final String DB_LOGIN_DEFAULT = "postgres";
    @NotNull
    public static final String DB_PASSWORD_DEFAULT = "123";
    @NotNull
    public static final String DB_HBM2DDL_AUTO_DEFAULT = "update";
    @NotNull
    public static final String DB_HBM2DDL_AUTO_KEY = "database.hbm2ddl_auto";
    @NotNull
    public static final String DB_LOGIN_KEY = "database.username";
    @NotNull
    public static final String DB_PASSWORD_KEY = "database.password";
    @NotNull
    public static final String DB_URL_KEY = "database.url";
    @NotNull
    public static final String DB_DRIVER_KEY = "database.driver";
    @NotNull
    public static final String DB_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";
    @NotNull
    public static final String DB_DRIVER_DEFAULT = "org.postgresql.Driver";
    @NotNull
    public static final String DB_DIALECT_KEY = "database.dialect";
    @NotNull
    public static final String DB_SHOW_SQL_DEFAULT = "false";
    @NotNull
    public static final String DB_SHOW_SQL_KEY = "database.show_sql";
    @NotNull
    public static final String DB_FORMAT_SQL_DEFAULT = "false";
    @NotNull
    public static final String DB_FORMAT_SQL_KEY = "database.format_sql";
    @NotNull
    public static final String DB_SECOND_LVL_CASH_DEFAULT = "true";
    @NotNull
    public static final String DB_SECOND_LVL_CASH_KEY = "database.second_lvl_cash";
    @NotNull
    public static final String DB_FACTORY_CLASS_DEFAULT = "com.hazelcast.hibernate.HazelcastCacheRegionFactory";
    @NotNull
    public static final String DB_FACTORY_CLASS_KEY = "database.factory_class";
    @NotNull
    public static final String DB_USE_QUERY_CASH_DEFAULT = "true";
    @NotNull
    public static final String DB_USE_QUERY_CASH_KEY = "database.use_query_cash";
    @NotNull
    public static final String DB_USE_MIN_PUTS_DEFAULT = "true";
    @NotNull
    public static final String DB_USE_MIN_PUTS_KEY = "database.use_min_puts";
    @NotNull
    public static final String DB_REGION_PREFIX_DEFAULT = "tm";
    @NotNull
    public static final String DB_REGION_PREFIX_KEY = "database.region_prefix";
    @NotNull
    public static final String DB_CONFIG_FILE_PATH_DEFAULT = "hazelcast.xml";
    @NotNull
    public static final String DB_CONFIG_FILE_PATH_KEY = "database.config_file_path";
    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";
    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";
    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";
    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";
    @NotNull
    private static final String DATABASE_FORMAT_SQL = "database.format_sql";
    @NotNull
    private static final String DATABASE_FORMAT_SQL_DEFAULT = "false";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String value = getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
        return Integer.parseInt(value);
    }

    @Override
    @NotNull
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT, SESSION_TIMEOUT_DEFAULT);
    }


    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().contains(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public String getDatabaseLogin() {
        return getStringValue(DB_LOGIN_KEY, DB_LOGIN_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DB_PASSWORD_KEY, DB_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getStringValue(DB_URL_KEY, DB_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DB_DRIVER_KEY, DB_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDialect() {
        return getStringValue(DB_DIALECT_KEY, DB_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseHBM2DDLAuto() {
        return getStringValue(DB_HBM2DDL_AUTO_KEY, DB_HBM2DDL_AUTO_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseShowSQL() {
        return getStringValue(DB_SHOW_SQL_KEY, DB_SHOW_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseFormatSQL() {
        return getStringValue(DB_FORMAT_SQL_KEY, DB_FORMAT_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseSecondLvlCash() {
        return getStringValue(DB_SECOND_LVL_CASH_KEY, DB_SECOND_LVL_CASH_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseFactoryClass() {
        return getStringValue(DB_FACTORY_CLASS_KEY, DB_FACTORY_CLASS_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseQueryCashKey() {
        return getStringValue(DB_USE_QUERY_CASH_KEY, DB_USE_QUERY_CASH_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUseMinPuts() {
        return getStringValue(DB_USE_MIN_PUTS_KEY, DB_USE_MIN_PUTS_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseRegionPrefix() {
        return getStringValue(DB_REGION_PREFIX_KEY, DB_REGION_PREFIX_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseConfigFilePath() {
        return getStringValue(DB_CONFIG_FILE_PATH_KEY, DB_CONFIG_FILE_PATH_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseFormatSql() {
        return getStringValue(DATABASE_FORMAT_SQL, DATABASE_FORMAT_SQL_DEFAULT);
    }

}