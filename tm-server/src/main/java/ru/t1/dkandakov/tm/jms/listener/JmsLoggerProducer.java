package ru.t1.dkandakov.tm.jms.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.jms.operation.OperationEvent;

import javax.jms.*;
import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class JmsLoggerProducer {

    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    private static final String QUEUE = "LOGGER";

    @NotNull
    private final BrokerService broker = new BrokerService();

    @NotNull
    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);

    @Nullable
    private final Connection connection;

    @Nullable
    private final Session session;

    @Nullable
    private final Queue destination;

    @Nullable
    private final MessageProducer messageProducer;

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @SneakyThrows
    public JmsLoggerProducer() {
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
        messageProducer = session.createProducer(destination);
    }

    @SneakyThrows
    public void send(@NotNull final String text) {
        @NotNull final TextMessage message = session.createTextMessage(text);
        messageProducer.send(message);
    }

    @SneakyThrows
    public void send(@NotNull final OperationEvent event) {
        executorService.submit(() -> sync(event));
    }

    @SneakyThrows
    public void sync(@NotNull final OperationEvent event) {
        @NotNull final Class<?> entityClass = event.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            @NotNull final Annotation annotation = entityClass.getAnnotation(Table.class);
            @NotNull final Table table = (Table) annotation;
            event.setTable(table.name());
        }
        send(objectWriter.writeValueAsString(event));
    }

}
