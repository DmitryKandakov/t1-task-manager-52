package ru.t1.dkandakov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.api.repository.model.IRepository;
import ru.t1.dkandakov.tm.model.AbstractModel;

import javax.persistence.EntityManager;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
